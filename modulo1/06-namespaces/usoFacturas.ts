import  {Facturas} from "./models"

console.log("Inicio uso programa...");
let facturas:Facturas.Factura[] = new Array();

let linea:Facturas.LineaFactura = new Facturas.LineaFactura(new Facturas.Producto(100,'manzana',1), 1);
let linea2:Facturas.LineaFactura = new Facturas.LineaFactura(new Facturas.Producto(101,'pera',2), 10);
let linea3:Facturas.LineaFactura = new Facturas.LineaFactura(new Facturas.Producto(102,'kiwi',3), 5);

facturas.push(new Facturas.FacturaIva([linea, linea2, linea3]));

function calcularImporteTotalFacturas (facturas:Facturas.Factura[])
{
    console.log(facturas.reduce((total, factura)=>total + factura.calcularImporte(),0));
}

facturas.push(new Facturas.FacturaSinIva([linea, linea2, linea3]));

calcularImporteTotalFacturas (facturas);