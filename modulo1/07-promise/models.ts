export interface Factura extends CalculoImporte
{
    lineasFactura: LineaFactura[];
    
    getLineas(): LineaFactura[];
    getLinea(i:number): LineaFactura;

}

export interface CalculoImporte
{
    calcularImporte(): number;
}

export class Producto
{
    codigo: number;
    descripcion: string;
    precio: number;

    constructor(codigo:number, descripcion: string, precio: number)
    {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
    }
}

export class LineaFactura implements CalculoImporte
{
    producto: Producto;
    numeroProductos: number;

    constructor(producto: Producto, numeroProductos: number)
    {
        this.producto = producto;
        this.numeroProductos = numeroProductos;
    }

    calcularImporte(): number
    {
        console.log(`calculando precio linea factura ${this.producto.codigo}...`);
        return this.producto.precio * this.numeroProductos;
    }
}

export class FacturaIva implements Factura
{
    lineasFactura: LineaFactura[];

    constructor(lineasFactura: LineaFactura[])
    {
        this.lineasFactura = lineasFactura;
    }

    getLineas(): LineaFactura[]
    {
        return this.lineasFactura;
    }

    getLinea(i:number): LineaFactura
    {
        return this.lineasFactura[i];
    }

    calcularImporte(): number
    {
        console.log("calculando factura con iva...");
        return this.lineasFactura.reduce((total, value) => value.calcularImporte() + total, 0) * 1.21;
    }
}

export class FacturaSinIva implements Factura
{
    lineasFactura: LineaFactura[];

    constructor(lineasFactura: LineaFactura[])
    {
        this.lineasFactura = lineasFactura;
    }

    getLineas(): LineaFactura[]
    {
        return this.lineasFactura;
    }

    getLinea(i:number): LineaFactura
    {
        return this.lineasFactura[i];
    }

    calcularImporte(): number
    {
        console.log("calculando factura sin iva...");
         return this.lineasFactura.reduce((total, value) => value.calcularImporte() + total, 0);
    }
}