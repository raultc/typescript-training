//error en opcionales
function add(x?: number, y: number=100): number {
    return x + y;
}
console.log(add(1,2));
console.log(add());

//error en opcionales y asignacion de valores por defecto
let myAdd = function(x?: number, y: number, z?:number = 100): number { return x + y + z; };
console.log(myAdd(1,2));
console.log(myAdd(1,22));
