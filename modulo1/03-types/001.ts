//boolean
var isFalse = false;
console.log(isFalse);

//string
var name = "Acadgild";
console.log(name);
//templated
var fullname = 'Yusuf Shakeel';
var outputSentence = `My name is ${ fullname }.`;
console.log(outputSentence);
var outputString = `1 + 2 = ${ 1 + 2 }`;
console.log("strings: ");

//number
var height = 99;
var g = 0b111001; // Binary
var h = 0o436; // Octal
var i = 0xadf0d; // Hexa-Decimal
console.log("numbers: ");

//array
var list = [1, 2, 3];
console.log("arrays: ");

//tuple
var userData = ['Yusuf Shakeel', true, 10];
console.log("tuplas: ");

//enum
enum stuff {pen, pencil, eraser};
var c= stuff.eraser;
//Enum starts numbering from 0, to change do this
enum Color {pen = 1, pencil, eraser};
var c = Color.pen;
console.log("Enums: ");

//any
var notSure = 4;
notSure = "maybe a string instead";
notSure = false; // okay, definitely a boolean
console.log("any: ");

var dynamicValue = "Kunal Chowdhury";
dynamicValue = 100;
dynamicValue = 0b1100101;
dynamicValue = true;
var dynamicList = [ "Kunal Chowdhury",
                           "Free User",
                           21,
                           true
                         ];
console.log("any: ");

//void
function warnUser(): void {
    console.log("This is my warning message");
}

//null -> object with no value 
//undefined -> not initialized var

