import { Factura, FacturaIva, LineaFactura, Producto, FacturaSinIva } from "./models";

console.log("Inicio uso programa...");
let facturas:Factura[] = new Array();

let linea:LineaFactura = new LineaFactura(new Producto(100,'manzana',1), 1);
let linea2:LineaFactura = new LineaFactura(new Producto(101,'pera',2), 10);
let linea3:LineaFactura = new LineaFactura(new Producto(102,'kiwi',3), 5);

facturas.push(new FacturaIva([linea, linea2, linea3]));

function calcularImporteTotalFacturas (facturas:Factura[])
{
    console.log(facturas.reduce((total, factura)=>total + factura.calcularImporte(),0));
}

facturas.push(new FacturaSinIva([linea, linea2, linea3]));

calcularImporteTotalFacturas (facturas);